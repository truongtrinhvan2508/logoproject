//
//  UserMainViewController.swift
//  LogoApp
//
//  Created by TruongTV2 on 4/25/22.
//

import UIKit

class UserMainViewController: UIViewController {

    @IBOutlet weak var loginSigupBtn: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var signupBtn: UIButton!
    @IBOutlet weak var loginSignupView: UIView!
    @IBOutlet weak var pageViewConstans: UIView!
    var senderTag: Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        setupPageViewController()
        setuplayout()
    }
    
    func setuplayout(){
        loginSignupView.addBottomBorderWithColor(color: UIColor(red: 0.659, green: 0.635, blue: 0.698, alpha: 0.3), width: 0.5)
        signupBtn.tintColor = UIColor.black
        loginBtn.tintColor = UIColor(red: 0.659, green: 0.635, blue: 0.698, alpha: 0.3)
        
    }
    
    private func setupPageViewController() {
        guard let pageViewController = UIStoryboard(name: "LoginSignup", bundle: nil).instantiateViewController(withIdentifier: "PageViewController") as? PageViewController else {return}
        
        addChild(pageViewController)
        pageViewController.view.layoutIfNeeded()
        pageViewConstans.addSubview(pageViewController.view)
        pageViewController.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            pageViewController.view.topAnchor.constraint(equalTo: pageViewConstans.topAnchor),
            pageViewController.view.leadingAnchor.constraint(equalTo: pageViewConstans.leadingAnchor),
            pageViewController.view.trailingAnchor.constraint(equalTo: pageViewConstans.trailingAnchor),
            pageViewController.view.bottomAnchor.constraint(equalTo: pageViewConstans.bottomAnchor)
        ])
        pageViewController.isPagingEnabled = false
        pageViewController.didMove(toParent: self)
    }
    
    @IBAction func invokeSetOption(_ sender: UIButton) {
        if sender.tag == 0{
            senderTag = 0
            loginSigupBtn.setTitle("Đăng Ký", for: .normal)
            signupBtn.tintColor = UIColor.black
            loginBtn.tintColor = UIColor(red: 0.659, green: 0.635, blue: 0.698, alpha: 0.3)
            NotificationCenter.default.post(name: Notification.Name("dangky/dangnhap"), object: nil, userInfo: ["key": 0])
        }else{
            senderTag = 1
            loginSigupBtn.setTitle("Đăng Nhập", for: .normal)
            loginBtn.tintColor = UIColor.black
            signupBtn.tintColor = UIColor(red: 0.659, green: 0.635, blue: 0.698, alpha: 0.3)
            NotificationCenter.default.post(name: Notification.Name("dangky/dangnhap"), object: nil, userInfo: ["key": 1])
        }
    }
    
    @IBAction func invokeLoginSignup(_ sender: Any) {
        if senderTag == 0{
            senderTag = 1
            loginSigupBtn.setTitle("Đăng Nhập", for: .normal)
            loginBtn.tintColor = UIColor.black
            signupBtn.tintColor = UIColor(red: 0.659, green: 0.635, blue: 0.698, alpha: 0.3)
            NotificationCenter.default.post(name: Notification.Name("dangky/dangnhap"), object: nil, userInfo: ["key": 1])
        }else{
            guard let vc = UIStoryboard(name: "TabbarViewController", bundle: nil).instantiateViewController(withIdentifier: "TabbarViewController") as? TabbarViewController else {return}
//            vc.modalPresentationStyle = .overFullScreen
//            self.present(vc, animated: true, completion: nil)
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}


