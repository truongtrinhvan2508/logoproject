//
//  ForgotPasswordViewController.swift
//  LogoApp
//
//  Created by TruongTV2 on 4/25/22.
//

import UIKit

class ForgotPasswordViewController: UIViewController {
    @IBOutlet weak var emailTxtFView: UIView!
    @IBOutlet weak var emailView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        setupLayoutView()
    }
    
    func setupLayoutView(){
        emailView.layer.borderWidth = 0.5
        emailView.layer.borderColor = UIColor(red: 0.659, green: 0.635, blue: 0.698, alpha: 0.3).cgColor
        emailTxtFView.addLeftBorderWithColor(color: UIColor(red: 0.659, green: 0.635, blue: 0.698, alpha: 0.3), width: 0.5)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func backBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
