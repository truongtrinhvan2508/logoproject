//
//  SignupViewController.swift
//  LogoApp
//
//  Created by TruongTV2 on 4/25/22.
//

import UIKit

class SignupViewController: UIViewController {
    
    @IBOutlet weak var passwordTxtFView: UIView!
    @IBOutlet weak var emailTxtFView: UIView!
    @IBOutlet weak var userTxtFView: UIView!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var userView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
        // Do any additional setup after loading the view.
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func setupLayout(){
        userView.layer.borderWidth = 0.5
        userView.layer.borderColor = UIColor(red: 0.659, green: 0.635, blue: 0.698, alpha: 0.3).cgColor
        userTxtFView.addLeftBorderWithColor(color: UIColor(red: 0.659, green: 0.635, blue: 0.698, alpha: 0.3), width: 0.5)
        emailView.layer.borderWidth = 0.5
        emailView.layer.borderColor = UIColor(red: 0.659, green: 0.635, blue: 0.698, alpha: 0.3).cgColor
        emailTxtFView.addLeftBorderWithColor(color: UIColor(red: 0.659, green: 0.635, blue: 0.698, alpha: 0.3), width: 0.5)
        passwordView.layer.borderWidth = 0.5
        passwordView.layer.borderColor = UIColor(red: 0.659, green: 0.635, blue: 0.698, alpha: 0.3).cgColor
        passwordTxtFView.addLeftBorderWithColor(color: UIColor(red: 0.659, green: 0.635, blue: 0.698, alpha: 0.3), width: 0.5)
    }
    
}
