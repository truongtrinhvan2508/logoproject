//
//  LoginViewController.swift
//  LogoApp
//
//  Created by TruongTV2 on 4/25/22.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var passwordTxtFView: UIView!
    @IBOutlet weak var passwordView: UIView!
    
    @IBOutlet weak var emailTxtFView: UIView!
    @IBOutlet weak var emailView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        setupLayoutView()
    }
    
    func setupLayoutView(){
        emailView.layer.borderWidth = 0.5
        emailView.layer.borderColor = UIColor(red: 0.659, green: 0.635, blue: 0.698, alpha: 0.3).cgColor
        emailTxtFView.addLeftBorderWithColor(color: UIColor(red: 0.659, green: 0.635, blue: 0.698, alpha: 0.3), width: 0.5)
        passwordView.layer.borderWidth = 0.5
        passwordView.layer.borderColor = UIColor(red: 0.659, green: 0.635, blue: 0.698, alpha: 0.3).cgColor
        passwordTxtFView.addLeftBorderWithColor(color: UIColor(red: 0.659, green: 0.635, blue: 0.698, alpha: 0.3), width: 0.5)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    @IBAction func invokeForgotPass(_ sender: Any) {
        let vc = UIStoryboard(name: "LoginSignup", bundle: nil).instantiateViewController(withIdentifier: "ForgotPasswordViewController") as? ForgotPasswordViewController ?? ForgotPasswordViewController()
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
}
