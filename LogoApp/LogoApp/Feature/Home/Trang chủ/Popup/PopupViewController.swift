//
//  PopupViewController.swift
//  LogoApp
//
//  Created by TruongTV2 on 4/26/22.
//

import UIKit


class PopupViewController: UIViewController {

    var stateSe: stateSelected = .no
    var indexSelectedState: Int = 0
    var listOption: [String] = ["Tham gia","Có thể tham gia","Không tham gia"]
    var indexPathRow: Int = 0
    var event: Events?
    var delegate: PopupDelegate?
    
    @IBOutlet weak var selectedOptionTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        // Do any additional setup after loading the view.
    }
    func setupTableView(){
        selectedOptionTableView.delegate = self
        selectedOptionTableView.dataSource = self
        selectedOptionTableView.register(UINib(nibName: "PopupTableViewCell", bundle: nil), forCellReuseIdentifier: "PopupTableViewCell")
    }

}

extension PopupViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listOption.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PopupTableViewCell", for: indexPath) as? PopupTableViewCell else { return UITableViewCell() }
        if indexPath.row == indexSelectedState{
            cell.selectedImg.isHidden = false
        }else{}
        cell.configueData(data: listOption[indexPath.row])
        return cell
    }
    
}
extension PopupViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dismiss(animated: true,completion: {
            switch indexPath.row{
            case 0:
                self.delegate?.popupDelegate(events: .sure, indexPathRow: self.indexPathRow)
            case 1:
                self.delegate?.popupDelegate(events: .notSure, indexPathRow: self.indexPathRow)
            default:
                self.delegate?.popupDelegate(events: .no, indexPathRow: self.indexPathRow)
            }
            
        })
    }
}

protocol PopupDelegate{
    func popupDelegate(events: stateSelected,indexPathRow: Int)
}
