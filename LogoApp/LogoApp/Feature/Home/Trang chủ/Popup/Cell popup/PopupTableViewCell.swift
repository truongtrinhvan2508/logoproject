//
//  PopupTableViewCell.swift
//  LogoApp
//
//  Created by TruongTV2 on 4/26/22.
//

import UIKit

class PopupTableViewCell: UITableViewCell {

    @IBOutlet weak var selectedImg: UIImageView!
    @IBOutlet weak var OptionLb: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectedImg.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configueData(data: String){
        OptionLb.text = data
    }
}
