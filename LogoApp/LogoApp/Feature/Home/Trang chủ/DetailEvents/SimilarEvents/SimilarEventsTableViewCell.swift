//
//  SimilarEventsTableViewCell.swift
//  LogoApp
//
//  Created by TruongTV2 on 4/28/22.
//

import UIKit

class SimilarEventsTableViewCell: UITableViewCell {

    @IBOutlet weak var similarEventsCollectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupCollectionView()
        selectionStyle = .none
    }

    func setupCollectionView(){
        
        similarEventsCollectionView.dataSource = self
        similarEventsCollectionView.delegate = self
        similarEventsCollectionView.register(UINib(nibName: "SimilarEventsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SimilarEventsCollectionViewCell")
    }
    
}

extension SimilarEventsTableViewCell: UICollectionViewDelegate{
    
}

extension SimilarEventsTableViewCell: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SimilarEventsCollectionViewCell", for: indexPath) as? SimilarEventsCollectionViewCell else {return UICollectionViewCell()}
                return cell
    }
    
}

extension SimilarEventsTableViewCell: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 335, height: self.similarEventsCollectionView.frame.height)
    }
}
