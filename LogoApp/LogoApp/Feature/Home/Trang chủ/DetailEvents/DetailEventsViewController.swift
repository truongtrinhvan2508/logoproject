//
//  DetailEventsViewController.swift
//  LogoApp
//
//  Created by TruongTV2 on 4/28/22.
//

import UIKit

class DetailEventsViewController: UIViewController {

    @IBOutlet weak var joinedImg: UIImageView!
    @IBOutlet weak var willBeJoinImg: UIImageView!
    @IBOutlet weak var detailEventsTableView: UITableView!
    
    var delegate: DetailEventsDelegate?
    var indexSelectedState: Int = 0
    var stateJoined: stateSelected = .no
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    func setupTableView(){
        switch stateJoined {
        case .no:
            willBeJoinImg.image = UIImage(named: "Rectangle 6-1")
            joinedImg.image = UIImage(named: "Rectangle 6-1")
        case .notSure:
            willBeJoinImg.image = UIImage(named: "Rectangle 6-2")
            joinedImg.image = UIImage(named: "Rectangle 6-1")
        case .sure:
            willBeJoinImg.image = UIImage(named: "Rectangle 6-1")
            joinedImg.image = UIImage(named: "Rectangle 6-2")
        }
        detailEventsTableView.rowHeight = UITableView.automaticDimension
        detailEventsTableView.estimatedRowHeight = 600
        detailEventsTableView.delegate = self
        detailEventsTableView.dataSource = self
        detailEventsTableView.register(UINib(nibName: "InfomationEventsTableViewCell", bundle: nil), forCellReuseIdentifier: "InfomationEventsTableViewCell")
        detailEventsTableView.register(UINib(nibName: "DescribeEventsTableViewCell", bundle: nil), forCellReuseIdentifier: "DescribeEventsTableViewCell")
        detailEventsTableView.register(UINib(nibName: "SimilarEventsTableViewCell", bundle: nil), forCellReuseIdentifier: "SimilarEventsTableViewCell")
    }
    
    @IBAction func invokeStateJoinedBtn(_ sender: UIButton) {
        
        if sender.tag == 0{
            stateJoined = .notSure
            willBeJoinImg.image = UIImage(named: "Rectangle 6-2")
            joinedImg.image = UIImage(named: "Rectangle 6-1")
        }else{
            stateJoined = .sure
            willBeJoinImg.image = UIImage(named: "Rectangle 6-1")
            joinedImg.image = UIImage(named: "Rectangle 6-2")
        }
    }
    @IBAction func invokeBackBtn(_ sender: Any) {
        dismiss(animated: true,completion: {
            self.delegate?.SetStateEvents(state: self.stateJoined, insexSelected: self.indexSelectedState)
        })
    }
    
}

extension DetailEventsViewController: UITableViewDelegate{
}

extension DetailEventsViewController: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section{
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "InfomationEventsTableViewCell", for: indexPath) as? InfomationEventsTableViewCell else { return UITableViewCell() }
            return cell
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DescribeEventsTableViewCell", for: indexPath) as? DescribeEventsTableViewCell else { return UITableViewCell() }
            return cell
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SimilarEventsTableViewCell", for: indexPath) as? SimilarEventsTableViewCell else { return UITableViewCell() }
            return cell
        }
    }
    
    
}

protocol DetailEventsDelegate{
    func SetStateEvents(state: stateSelected,insexSelected: Int)
}
