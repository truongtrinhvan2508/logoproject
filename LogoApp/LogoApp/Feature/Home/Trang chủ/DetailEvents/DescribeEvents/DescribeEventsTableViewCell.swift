//
//  DescribeEventsTableViewCell.swift
//  LogoApp
//
//  Created by TruongTV2 on 4/28/22.
//

import UIKit

class DescribeEventsTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
