//
//  HomeViewController.swift
//  LogoApp
//
//  Created by TruongTV2 on 4/26/22.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var selectedEventsImg: UIImageView!
    @IBOutlet weak var selectedNewsImg: UIImageView!
    @IBOutlet weak var optionView: UIView!
    @IBOutlet weak var eventsBtn: UIButton!
    @IBOutlet weak var newsBtn: UIButton!
    @IBOutlet weak var pageHomeView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setuplayout()
        setupPageViewController()
    }
    
    func setuplayout(){
        optionView.addBottomBorderWithColor(color: UIColor(red: 0.659, green: 0.635, blue: 0.698, alpha: 0.3), width: 0.5)
        newsBtn.tintColor = UIColor(red: 0.365, green: 0.125, blue: 0.804, alpha: 1)
        eventsBtn.tintColor = UIColor(red: 0.659, green: 0.635, blue: 0.698, alpha: 1)
        
    }
    
    private func setupPageViewController() {
        guard let pageViewController = UIStoryboard(name: "HomeViewController", bundle: nil).instantiateViewController(withIdentifier: "HomePageViewController") as? HomePageViewController else {return}
        
        addChild(pageViewController)
        pageViewController.view.layoutIfNeeded()
        pageHomeView.addSubview(pageViewController.view)
        pageViewController.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            pageViewController.view.topAnchor.constraint(equalTo: pageHomeView.topAnchor),
            pageViewController.view.leadingAnchor.constraint(equalTo: pageHomeView.leadingAnchor),
            pageViewController.view.trailingAnchor.constraint(equalTo: pageHomeView.trailingAnchor),
            pageViewController.view.bottomAnchor.constraint(equalTo: pageHomeView.bottomAnchor)
        ])
        pageViewController.isPagingEnabled = false
        pageViewController.didMove(toParent: self)
    }
    
    @IBAction func invokeChoseOption(_ sender: UIButton) {
        if sender.tag == 0{
            newsBtn.tintColor = UIColor(red: 0.365, green: 0.125, blue: 0.804, alpha: 1)
            eventsBtn.tintColor = UIColor(red: 0.659, green: 0.635, blue: 0.698, alpha: 1)
            selectedNewsImg.isHidden = false
            selectedEventsImg.isHidden = true
            NotificationCenter.default.post(name: Notification.Name("HomePage"), object: nil, userInfo: ["page": 0])
        }else{
            eventsBtn.tintColor = UIColor(red: 0.365, green: 0.125, blue: 0.804, alpha: 1)
            newsBtn.tintColor = UIColor(red: 0.659, green: 0.635, blue: 0.698, alpha: 1)
            selectedNewsImg.isHidden = true
            selectedEventsImg.isHidden = false
            NotificationCenter.default.post(name: Notification.Name("HomePage"), object: nil, userInfo: ["page": 1])
        }
    }
}
