//
//  EventsViewController.swift
//  LogoApp
//
//  Created by TruongTV2 on 4/26/22.
//

import UIKit

class EventsViewController: UIViewController {
    
    @IBOutlet weak var eventsTableView: UITableView!
    
    var dataEvents: [Events] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }


    func setupTableView(){
        dataEvents = EventsRepository.Shared.EventsData()
        eventsTableView.delegate = self
        eventsTableView.dataSource = self
        eventsTableView.register(UINib(nibName: "EvenstTableViewCell", bundle: nil), forCellReuseIdentifier: "EvenstTableViewCell")
    }

}

extension EventsViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let vc = UIStoryboard(name: "DetailEventsViewController", bundle: nil).instantiateViewController(withIdentifier: "DetailEventsViewController") as? DetailEventsViewController else {return}
        vc.delegate = self
        vc.indexSelectedState = indexPath.row
        vc.stateJoined = dataEvents[indexPath.row].state
        vc.modalPresentationStyle = .overFullScreen
        present(vc, animated: true)
    }
    
}

extension EventsViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataEvents.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "EvenstTableViewCell", for: indexPath) as? EvenstTableViewCell else { return UITableViewCell() }
        cell.delegate = self
        cell.indexPathRowEv = indexPath.row
        cell.configueData(data: dataEvents[indexPath.row])
        return cell
    }
    
    
}
extension EventsViewController: EvenstDelegate{
    func presetDelegate(state: stateSelected, indexStateEv: Int, indexPathRow: Int) {
        guard let vc = UIStoryboard(name: "PopupViewController", bundle: nil).instantiateViewController(withIdentifier: "PopupViewController") as? PopupViewController else { return }
        vc.stateSe = state
        vc.indexSelectedState = indexStateEv
        vc.indexPathRow = indexPathRow
        vc.delegate = self
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        present(vc, animated: true, completion: nil)
    }
}
extension EventsViewController: PopupDelegate {
    func popupDelegate(events: stateSelected, indexPathRow: Int) {
        dataEvents[indexPathRow].state = events
        eventsTableView.reloadData()
    }
}

extension EventsViewController: DetailEventsDelegate{
    func SetStateEvents(state: stateSelected, insexSelected: Int) {
        dataEvents[insexSelected].state = state
        eventsTableView.reloadData()
    }
    
    
}
