//
//  EvenstTableViewCell.swift
//  LogoApp
//
//  Created by TruongTV2 on 4/26/22.
//

import UIKit


class EvenstTableViewCell: UITableViewCell {
    
    // MARK: IBOutlets
    @IBOutlet weak var dateEventsLb: UILabel!
    @IBOutlet weak var quantityEventsLb: UILabel!
    @IBOutlet weak var titleEventsLb: UILabel!
    @IBOutlet weak var descriptionEventLb: UILabel!
    @IBOutlet weak var acceptEventBtn: UIButton!
    
    // MARK: Variables
    var check: Bool = false
    var delegate: EvenstDelegate?
    var indexPathEv: Int = 0
    var indexPathRowEv: Int = 0
    var state: stateSelected = .sure
    
    // MARK: Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        // Initialization code
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()

        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))
        contentView.layer.cornerRadius = 30
    }
    
    // MARK: Functions
    func configueData(data: Events){
        dateEventsLb.text = data.date
        titleEventsLb.text = data.title
        quantityEventsLb.text = "\(data.quantity) người tham gia"
        descriptionEventLb.text = data.description
        state = data.state
        switch state {
        case .no:
            indexPathEv = 2
            acceptEventBtn.setImage(UIImage(named: "unaccept"), for: .normal)
        case .notSure:
            indexPathEv = 1
            acceptEventBtn.setImage(UIImage(named: "accept"), for: .normal)
        case .sure:
            indexPathEv = 0
            acceptEventBtn.setImage(UIImage(named: "joined"), for: .normal)
        }
    }
    @IBAction func acceptEvents(_ sender: UIButton) {
        delegate?.presetDelegate(state: state,indexStateEv: indexPathEv, indexPathRow: indexPathRowEv)
    }
    
}

protocol EvenstDelegate{
    func presetDelegate(state: stateSelected,indexStateEv: Int,indexPathRow: Int)
}


