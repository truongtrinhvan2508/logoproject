//
//  NewsTableViewCell.swift
//  LogoApp
//
//  Created by TruongTV2 on 4/26/22.
//

import UIKit

class NewsTableViewCell: UITableViewCell {

    @IBOutlet weak var addressNewsLb: UILabel!
    @IBOutlet weak var authorNewsLb: UILabel!
    @IBOutlet weak var titleNewsLb: UILabel!
    @IBOutlet weak var dateNewsLb: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()

        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))
        contentView.layer.cornerRadius = 30
    }

    func configueData(data: News){
        dateNewsLb.text = data.date
        titleNewsLb.text = data.title
        authorNewsLb.text = data.author
        addressNewsLb.text = data.address
    }
}
