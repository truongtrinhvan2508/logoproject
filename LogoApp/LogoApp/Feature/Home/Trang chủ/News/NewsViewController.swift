//
//  NewsViewController.swift
//  LogoApp
//
//  Created by TruongTV2 on 4/26/22.
//

import UIKit

class NewsViewController: UIViewController {

    var dataNews: [News] = []
    @IBOutlet weak var NewsTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTableView()
    }

    func setupTableView(){
        dataNews = NewsRepository.shared.NewsData()
        NewsTableView.delegate = self
        NewsTableView.dataSource = self
        NewsTableView.register(UINib(nibName: "NewsTableViewCell", bundle: nil), forCellReuseIdentifier: "NewsTableViewCell")
    }

}

extension NewsViewController: UITableViewDelegate{
    
}

extension NewsViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataNews.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "NewsTableViewCell", for: indexPath) as? NewsTableViewCell else { return UITableViewCell() }
        cell.configueData(data: dataNews[indexPath.row])
        return cell
    }
    
    
}
