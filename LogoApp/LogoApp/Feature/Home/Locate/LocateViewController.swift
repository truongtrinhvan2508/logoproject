//
//  LocateViewController.swift
//  LogoApp
//
//  Created by TruongTV2 on 4/26/22.
//

import UIKit
import MapKit

struct EventsLocate{
    var name: String
    var subTitle: String
    var date: String
    var quantity: Int
    var coordinate2d: CLLocationCoordinate2D
    
}

class LocateViewController: UIViewController {
    
    @IBOutlet weak var detailEventView: UIView!
    @IBOutlet weak var quantityEventsLocateLb: UILabel!
    @IBOutlet weak var dateEventsLacateLb: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    
    let userLocate: String = "Vị trí của tôi"
    let locationManager = CLLocationManager()
    let eventsLocate = [EventsLocate(name: "Tòa nhà Sông Đà", subTitle: "Division 1", date: "Thứ 4, 2022/07/21", quantity: 10, coordinate2d: CLLocationCoordinate2D(latitude: 21.0176356, longitude: 105.7809957)),
                        EventsLocate(name: "Handico Tower", subTitle: "Rikkeisoft", date: "Thứ 4, 2022/09/14", quantity: 20, coordinate2d: CLLocationCoordinate2D(latitude: 21.01685, longitude: 105.7817716)),
                        EventsLocate(name: "TheSun Me tri", subTitle: "ROIS Premium Workspace", date: "Thứ 4, 2022/10/11", quantity: 30, coordinate2d: CLLocationCoordinate2D(latitude: 21.0157552, longitude: 105.7804213)),
                        EventsLocate(name: "Vinhomes SkyLake", subTitle: "Chung cư", date: "Thứ 4, 2022/09/14", quantity: 0, coordinate2d: CLLocationCoordinate2D(latitude: 21.0200933, longitude: 105.7820991)),
                        EventsLocate(name: "FLC Complex", subTitle: "Chung cư", date: "Thứ 4, 2022/09/14", quantity: 30, coordinate2d: CLLocationCoordinate2D(latitude: 21.0271586, longitude: 105.7784945))]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkLocationAuthorization()
        setupUI()
        fetchStadiumsOnMap(eventsLocate)
    }
    
    func setupUI(){
        detailEventView.isHidden = true
        mapView.mapType = .standard
        mapView.delegate = self
    }
    
    func checkLocationServices() {
        if CLLocationManager.locationServicesEnabled() {
            checkLocationAuthorization()
        } else {
            // Show alert letting the user know they have to turn this on.
        }
    }
    func checkLocationAuthorization() {
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            mapView.showsUserLocation = true
        case .denied: // Show alert telling users how to turn on permissions
            break
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            mapView.showsUserLocation = true
        case .restricted: // Show an alert letting them know what’s up
            break
        case .authorizedAlways:
            break
        }
    }
    
    func fetchStadiumsOnMap(_ stadiums: [EventsLocate]) {
        
        let userAnnotation = MKPointAnnotation()
        userAnnotation.title = userLocate
        userAnnotation.coordinate = CLLocationCoordinate2D(latitude: 21.016615, longitude: 105.780384)
        mapView.addAnnotation(userAnnotation)
        for eventsLocate in eventsLocate {
            let annotations = MKPointAnnotation()
            annotations.title = eventsLocate.name
            annotations.subtitle = eventsLocate.subTitle
            annotations.coordinate = CLLocationCoordinate2D(latitude: eventsLocate.coordinate2d.latitude, longitude: eventsLocate.coordinate2d.longitude)
            let region = MKCoordinateRegion(center: annotations.coordinate, latitudinalMeters: 300, longitudinalMeters: 300)
            mapView.setRegion(region, animated: true)
            
            if ((userAnnotation.coordinate.latitude - annotations.coordinate.latitude) * 111) <= 1 || ((userAnnotation.coordinate.longitude - annotations.coordinate.longitude) * 111) <= 1{
                
                mapView.addAnnotation(annotations)
            }
        }
        
        mapView.setCenter(userAnnotation.coordinate, animated: true)
    }
}

extension LocateViewController: MKMapViewDelegate{
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        let annotation = view.annotation
        if annotation?.title == userLocate{
            detailEventView.isHidden = true
        }else{
            detailEventView.isHidden = false
            for eventsLocate in eventsLocate {
                if annotation?.title == eventsLocate.name{
                    dateEventsLacateLb.text = eventsLocate.date
                    quantityEventsLocateLb.text = "\(eventsLocate.quantity) sẽ tham gia"
                }else{
                    ////
                }
            }
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard annotation is MKPointAnnotation else {
            return nil
        }
        
        let identifier = "locationAnnotation"
        let annotationView: MKPinAnnotationView
        
        if let dequeView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKPinAnnotationView {
            annotationView = dequeView
        } else {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView.canShowCallout = true
        }
        var image: UIImage{
            if annotation.title == userLocate{
                return UIImage(named: "selectedPointLocate")!
            }
            return UIImage(named: "locatePoint")!
        }
        annotationView.image = image
        
        return annotationView
    }
    
//    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
//        if let polyline = overlay as? MKPolyline {
//            let renderer = MKPolylineRenderer(polyline: polyline)
//            renderer.strokeColor = UIColor.blue
//            renderer.lineWidth = 3
//            return renderer
//
//        } else if let circle = overlay as? MKCircle {
//            let circleRenderer = MKCircleRenderer(circle: circle)
//            circleRenderer.fillColor = UIColor(red: 0, green: 0, blue: 1, alpha: 0.5)
//            circleRenderer.strokeColor = .blue
//            circleRenderer.lineWidth = 1
//            circleRenderer.lineDashPhase = 10
//            return circleRenderer
//
//        } else {
//            return MKOverlayRenderer()
//        }
//    }
}
