//
//  TabbarViewController.swift
//  LogoApp
//
//  Created by TruongTV2 on 4/26/22.
//
import Foundation
import UIKit

class TabbarViewController: UITabBarController {
    var customTabBarView = UIView(frame: .zero)
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBar.backgroundColor = .white
        viewControllers = [makeNavigationHome(),makeNavigationLocate(),makeNavigationBrowse(),makeNavigationProfile()]
        tabBar.tintColor = UIColor(red: 0.534, green: 0.311, blue: 0.946, alpha: 1)
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
    
    private func makeNavigationHome() -> UINavigationController {
        let vc = UIStoryboard(name: "HomeViewController", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        vc.tabBarItem = UITabBarItem(title: "Trang chủ", image: UIImage(named: "icon-home"), tag: 1)
        let navi = UINavigationController(rootViewController: vc)
        navi.navigationBar.isHidden = true
        return navi
    }
    private func makeNavigationLocate() -> UINavigationController {
        let vc = UIStoryboard(name: "LocateViewController", bundle: nil).instantiateViewController(withIdentifier: "LocateViewController") as! LocateViewController
        vc.tabBarItem = UITabBarItem(title: "Gần tôi", image: UIImage(named: "icon-located"), tag: 2)
        let navi = UINavigationController(rootViewController: vc)
        navi.navigationBar.isHidden = true
        return navi
    }
    private func makeNavigationBrowse() -> UINavigationController {
        let vc = UIStoryboard(name: "BrowseViewController", bundle: nil).instantiateViewController(withIdentifier: "BrowseViewController") as! BrowseViewController
        vc.tabBarItem = UITabBarItem(title: "Danh mục", image: UIImage(named: "icon-browse"), tag: 3)
        let navi = UINavigationController(rootViewController: vc)
        navi.navigationBar.isHidden = true
        return navi
    }
    private func makeNavigationProfile() -> UINavigationController {
        let vc = UIStoryboard(name: "ProfileViewController", bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        vc.tabBarItem = UITabBarItem(title: "Cá nhân", image: UIImage(named: "icon-profile"), tag: 4)
        let navi = UINavigationController(rootViewController: vc)
        navi.navigationBar.isHidden = true
        return navi
    }
    
}
