//
//  DetailBrowseViewController.swift
//  LogoApp
//
//  Created by TruongTV2 on 4/27/22.
//

import UIKit

class DetailBrowseViewController: UIViewController {

    @IBOutlet weak var pageBrowseView: UIView!
    @IBOutlet weak var LatestSelectedImg: UIImageView!
    @IBOutlet weak var popularSelectedImg: UIImageView!
    @IBOutlet weak var LatestBtn: UIButton!
    @IBOutlet weak var popularBtn: UIButton!
    @IBOutlet weak var optionView: UIView!
    @IBOutlet weak var categoryLb: UILabel!
    
    
    var category: String = ""
    var dataEvents: [Events] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        setuplayout()
        setupPageViewController()
    }
    func setuplayout(){
        dataEvents = EventsRepository.Shared.EventsData()
        categoryLb.text = "\(category) (\(dataEvents.count))"
        optionView.addBottomBorderWithColor(color: UIColor(red: 0.659, green: 0.635, blue: 0.698, alpha: 0.3), width: 0.5)
        popularBtn.tintColor = UIColor(red: 0.365, green: 0.125, blue: 0.804, alpha: 1)
        LatestBtn.tintColor = UIColor(red: 0.659, green: 0.635, blue: 0.698, alpha: 1)
        
    }
    
    private func setupPageViewController() {
        guard let pageViewController = UIStoryboard(name: "DetailBrowseViewController", bundle: nil).instantiateViewController(withIdentifier: "DetailBrowsePageViewController") as? DetailBrowsePageViewController else {return}
        
        addChild(pageViewController)
        pageViewController.view.layoutIfNeeded()
        pageBrowseView.addSubview(pageViewController.view)
        pageViewController.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            pageViewController.view.topAnchor.constraint(equalTo: pageBrowseView.topAnchor),
            pageViewController.view.leadingAnchor.constraint(equalTo: pageBrowseView.leadingAnchor),
            pageViewController.view.trailingAnchor.constraint(equalTo: pageBrowseView.trailingAnchor),
            pageViewController.view.bottomAnchor.constraint(equalTo: pageBrowseView.bottomAnchor)
        ])
        
        pageViewController.didMove(toParent: self)
    }
    
    @IBAction func invokeChoseOption(_ sender: UIButton) {
        if sender.tag == 0{
            popularBtn.tintColor = UIColor(red: 0.365, green: 0.125, blue: 0.804, alpha: 1)
            LatestBtn.tintColor = UIColor(red: 0.659, green: 0.635, blue: 0.698, alpha: 1)
            popularSelectedImg.isHidden = false
            LatestSelectedImg.isHidden = true
            NotificationCenter.default.post(name: Notification.Name("BrowsePage"), object: nil, userInfo: ["Page": 0])
        }else{
            LatestBtn.tintColor = UIColor(red: 0.365, green: 0.125, blue: 0.804, alpha: 1)
            popularBtn.tintColor = UIColor(red: 0.659, green: 0.635, blue: 0.698, alpha: 1)
            popularSelectedImg.isHidden = true
            LatestSelectedImg.isHidden = false
            NotificationCenter.default.post(name: Notification.Name("BrowsePage"), object: nil, userInfo: ["Page": 1])
        }
    }
    @IBAction func invokeBackBtn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
}
