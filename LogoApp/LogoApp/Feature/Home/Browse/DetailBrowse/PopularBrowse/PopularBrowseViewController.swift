//
//  PopularBrowseViewController.swift
//  LogoApp
//
//  Created by TruongTV2 on 4/27/22.
//

import UIKit

class PopularBrowseViewController: UIViewController {

    @IBOutlet weak var PopularTableView: UITableView!
    
    var dataPopularBrowse: [Events] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }


    func setupTableView(){
        dataPopularBrowse = EventsRepository.Shared.EventsData()
        PopularTableView.delegate = self
        PopularTableView.dataSource = self
        PopularTableView.register(UINib(nibName: "EvenstTableViewCell", bundle: nil), forCellReuseIdentifier: "EvenstTableViewCell")
    }

}

extension PopularBrowseViewController: UITableViewDelegate{
    
}

extension PopularBrowseViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataPopularBrowse.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "EvenstTableViewCell", for: indexPath) as? EvenstTableViewCell else { return UITableViewCell() }
        cell.delegate = self
        cell.indexPathRowEv = indexPath.row
        cell.configueData(data: dataPopularBrowse[indexPath.row])
        return cell
    }
}
extension PopularBrowseViewController: EvenstDelegate{
    func presetDelegate(state: stateSelected, indexStateEv: Int, indexPathRow: Int) {
        guard let vc = UIStoryboard(name: "PopupViewController", bundle: nil).instantiateViewController(withIdentifier: "PopupViewController") as? PopupViewController else { return }
        vc.stateSe = state
        vc.indexSelectedState = indexStateEv
        vc.indexPathRow = indexPathRow
        vc.delegate = self
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        present(vc, animated: true, completion: nil)
    }
}
extension PopularBrowseViewController: PopupDelegate {
    func popupDelegate(events: stateSelected, indexPathRow: Int) {
        dataPopularBrowse[indexPathRow].state = events
        PopularTableView.reloadData()
    }
}
