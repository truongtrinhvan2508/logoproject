//
//  LatestBrowseViewController.swift
//  LogoApp
//
//  Created by TruongTV2 on 4/27/22.
//

import UIKit

class LatestBrowseViewController: UIViewController {

    @IBOutlet weak var LatestTableView: UITableView!
    
    var dataLatestBrowse: [Events] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }


    func setupTableView(){
        dataLatestBrowse = EventsRepository.Shared.EventsData()
        LatestTableView.delegate = self
        LatestTableView.dataSource = self
        LatestTableView.register(UINib(nibName: "EvenstTableViewCell", bundle: nil), forCellReuseIdentifier: "EvenstTableViewCell")
    }

}

extension LatestBrowseViewController: UITableViewDelegate{
    
}

extension LatestBrowseViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataLatestBrowse.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "EvenstTableViewCell", for: indexPath) as? EvenstTableViewCell else { return UITableViewCell() }
        cell.delegate = self
        cell.indexPathRowEv = indexPath.row
        cell.configueData(data: dataLatestBrowse[indexPath.row])
        return cell
    }
    
}
extension LatestBrowseViewController: EvenstDelegate{
    func presetDelegate(state: stateSelected, indexStateEv: Int, indexPathRow: Int) {
        guard let vc = UIStoryboard(name: "PopupViewController", bundle: nil).instantiateViewController(withIdentifier: "PopupViewController") as? PopupViewController else { return }
        vc.stateSe = state
        vc.indexSelectedState = indexStateEv
        vc.indexPathRow = indexPathRow
        vc.delegate = self
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        present(vc, animated: true, completion: nil)
    }
}
extension LatestBrowseViewController: PopupDelegate {
    func popupDelegate(events: stateSelected, indexPathRow: Int) {
        dataLatestBrowse[indexPathRow].state = events
        LatestTableView.reloadData()
    }
}
