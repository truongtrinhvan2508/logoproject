//
//  BrowseViewController.swift
//  LogoApp
//
//  Created by TruongTV2 on 4/26/22.
//

import UIKit

class BrowseViewController: UIViewController {

    @IBOutlet weak var BrowseTableView: UITableView!
    
    var dataBrowse: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    func setupTableView(){
        dataBrowse = BrowseRepository.shared.BrowseData()
        BrowseTableView.dataSource = self
        BrowseTableView.delegate = self
        BrowseTableView.register(UINib(nibName: "BrowseTableViewCell", bundle: nil), forCellReuseIdentifier: "BrowseTableViewCell")
    }

    @IBAction func invokeSearch(_ sender: UIButton) {
        guard let vc = UIStoryboard(name: "SearchBrowseViewController", bundle: nil).instantiateViewController(withIdentifier: "SearchBrowseViewController") as? SearchBrowseViewController else {return}
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension BrowseViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let vc = UIStoryboard(name: "DetailBrowseViewController", bundle: nil).instantiateViewController(withIdentifier: "DetailBrowseViewController") as? DetailBrowseViewController else {return}
        vc.category = dataBrowse[indexPath.row]
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension BrowseViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataBrowse.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "BrowseTableViewCell", for: indexPath) as? BrowseTableViewCell else { return UITableViewCell() }
        cell.configueData(data: dataBrowse[indexPath.row])
        return cell
    }
    
}
