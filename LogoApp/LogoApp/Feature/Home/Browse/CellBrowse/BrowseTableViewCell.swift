//
//  BrowseTableViewCell.swift
//  LogoApp
//
//  Created by TruongTV2 on 4/27/22.
//

import UIKit

class BrowseTableViewCell: UITableViewCell {

    @IBOutlet weak var iconBrowseImg: UIImageView!
    @IBOutlet weak var titleBrowseLb: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }

    func configueData(data: String){
        titleBrowseLb.text = data
        iconBrowseImg.image = UIImage(named: data)
    }
}
