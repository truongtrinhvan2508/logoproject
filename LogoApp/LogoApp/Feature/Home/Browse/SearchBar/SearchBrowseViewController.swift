//
//  SearchBrowseViewController.swift
//  LogoApp
//
//  Created by TruongTV2 on 4/28/22.
//

import UIKit

class SearchBrowseViewController: UIViewController {
    
    @IBOutlet weak var searchBarTxt: UITextField!
    @IBOutlet weak var searchBrowseTableView: UITableView!
    
    var dataBrowse: [String] = []
    var searchArrBrowse: [String] = []
    var searching:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
//        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
//        view.addGestureRecognizer(tap)
    }
    
    func setupTableView(){
        dataBrowse = BrowseRepository.shared.BrowseData()
        searchBarTxt.delegate = self
        searchBrowseTableView.dataSource = self
        searchBrowseTableView.delegate = self
        searchBrowseTableView.register(UINib(nibName: "BrowseTableViewCell", bundle: nil), forCellReuseIdentifier: "BrowseTableViewCell")
    }
    
    @IBAction func invokeBackBtn(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
}
extension SearchBrowseViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let vc = UIStoryboard(name: "DetailSearchBarViewController", bundle: nil).instantiateViewController(withIdentifier: "DetailSearchBarViewController") as? DetailSearchBarViewController else {return}
        if( searching == true){
            vc.category = searchArrBrowse[indexPath.row]
        }else{
            vc.category = dataBrowse[indexPath.row]
        }
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension SearchBrowseViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if( searching == true){
            return searchArrBrowse.count
        }else{
            return dataBrowse.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "BrowseTableViewCell", for: indexPath) as? BrowseTableViewCell else { return UITableViewCell() }
        if( searching == true){
            cell.configueData(data: searchArrBrowse[indexPath.row])
        }else{
            cell.configueData(data: dataBrowse[indexPath.row])
        }
        return cell
    }
    
}
extension SearchBrowseViewController: UITextFieldDelegate{

    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        let searchText  = searchBarTxt.text! + string
        searchArrBrowse = dataBrowse.filter({ (result) -> Bool in
            return result.range(of: searchText, options: .caseInsensitive) != nil
        })
        if(searchArrBrowse.count == 0){
            searching = false
        } else {
            searching = true
        }
        self.searchBrowseTableView.reloadData()
        
        return true
    }
}

