//
//  PageDetailSearchBarViewController.swift
//  LogoApp
//
//  Created by TruongTV2 on 4/28/22.
//

import UIKit

class PageDetailSearchBarViewController: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    private lazy var subViewController:[UIViewController] = {
        return [
            UIStoryboard(name: "TookPlaceViewController", bundle: nil).instantiateViewController(withIdentifier: "TookPlaceViewController") as? TookPlaceViewController ?? TookPlaceViewController(),
            UIStoryboard(name: "UpComingViewController", bundle: nil).instantiateViewController(withIdentifier: "UpComingViewController") as? UpComingViewController ?? UpComingViewController()
        ]
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.dataSource = self
        setViewControllers([subViewController[0]], direction: .forward, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerNotification()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        removeNotification()
    }
    
    required init?(coder: NSCoder) {
        super.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return subViewController.count
    }
    
    private func removeNotification() {
        NotificationCenter.default.removeObserver(Notification.Name("SearchPage"))
    }

    
    private func registerNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.receivedNotification(notification:)), name: Notification.Name("SearchPage"), object: nil)
    }
    
    @objc private func receivedNotification(notification: Notification) {
        if let position = notification.userInfo?["searchKey"] as? Int {
            if position < subViewController.count {
                setViewControllers([subViewController[position]], direction: .forward, animated: false, completion: nil)
            }
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let currentIndex:Int = subViewController.firstIndex(of: viewController) ?? 0
        if(currentIndex <= 0) {
            return nil
        }
        return subViewController[currentIndex-1]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let currentIndex:Int = subViewController.firstIndex(of: viewController) ?? 0
        if(currentIndex >= subViewController.count-1) {
            return nil
        }
        return subViewController[currentIndex+1]
    }


}
