//
//  DetailSearchBarViewController.swift
//  LogoApp
//
//  Created by TruongTV2 on 4/28/22.
//

import UIKit

class DetailSearchBarViewController: UIViewController {

    @IBOutlet weak var categoryTxt: UITextField!
    @IBOutlet weak var selectedUpComingImg: UIImageView!
    @IBOutlet weak var selectedTookPlaceImg: UIImageView!
    @IBOutlet weak var optionView: UIView!
    @IBOutlet weak var tookPlaceBtn: UIButton!
    @IBOutlet weak var upcomingBtn: UIButton!
    @IBOutlet weak var pageSearchView: UIView!
    
    var category: String = ""
    var dataEvents: [Events] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        setuplayout()
        setupPageViewController()
    }
    func setuplayout(){
        categoryTxt.text = category
        dataEvents = EventsRepository.Shared.EventsData()
        tookPlaceBtn.setTitle("Đá diễn ra (\(dataEvents.count))", for: .normal)
        upcomingBtn.setTitle("Sắp diễn ra (\(dataEvents.count))", for: .normal)
        optionView.addBottomBorderWithColor(color: UIColor(red: 0.659, green: 0.635, blue: 0.698, alpha: 0.3), width: 0.5)
        tookPlaceBtn.tintColor = UIColor(red: 0.365, green: 0.125, blue: 0.804, alpha: 1)
        upcomingBtn.tintColor = UIColor(red: 0.659, green: 0.635, blue: 0.698, alpha: 1)
        
    }
    
    private func setupPageViewController() {
        guard let pageViewController = UIStoryboard(name: "DetailSearchBarViewController", bundle: nil).instantiateViewController(withIdentifier: "PageDetailSearchBarViewController") as? PageDetailSearchBarViewController else {return}
        
        addChild(pageViewController)
        pageViewController.view.layoutIfNeeded()
        pageSearchView.addSubview(pageViewController.view)
        pageViewController.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            pageViewController.view.topAnchor.constraint(equalTo: pageSearchView.topAnchor),
            pageViewController.view.leadingAnchor.constraint(equalTo: pageSearchView.leadingAnchor),
            pageViewController.view.trailingAnchor.constraint(equalTo: pageSearchView.trailingAnchor),
            pageViewController.view.bottomAnchor.constraint(equalTo: pageSearchView.bottomAnchor)
        ])
        
        pageViewController.didMove(toParent: self)
    }

    @IBAction func invokeOption(_ sender: UIButton) {
        if sender.tag == 0{
            tookPlaceBtn.tintColor = UIColor(red: 0.365, green: 0.125, blue: 0.804, alpha: 1)
            upcomingBtn.tintColor = UIColor(red: 0.659, green: 0.635, blue: 0.698, alpha: 1)
            selectedTookPlaceImg.isHidden = false
            selectedUpComingImg.isHidden = true
            NotificationCenter.default.post(name: Notification.Name("SearchPage"), object: nil, userInfo: ["searchKey": 0])
        }else{
            upcomingBtn.tintColor = UIColor(red: 0.365, green: 0.125, blue: 0.804, alpha: 1)
            tookPlaceBtn.tintColor = UIColor(red: 0.659, green: 0.635, blue: 0.698, alpha: 1)
            selectedUpComingImg.isHidden = false
            selectedTookPlaceImg.isHidden = true
            NotificationCenter.default.post(name: Notification.Name("SearchPage"), object: nil, userInfo: ["searchKey": 1])
        }
    }
    @IBAction func invokeBackBtn(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}
