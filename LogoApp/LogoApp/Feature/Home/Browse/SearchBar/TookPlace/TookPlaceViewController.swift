//
//  TookPlaceViewController.swift
//  LogoApp
//
//  Created by TruongTV2 on 4/28/22.
//

import UIKit

class TookPlaceViewController: UIViewController {

    @IBOutlet weak var tookPlaceTableView: UITableView!
    var dataTookPlace: [Events] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }

    func setupTableView(){
        dataTookPlace = EventsRepository.Shared.EventsData()
        tookPlaceTableView.delegate = self
        tookPlaceTableView.dataSource = self
        tookPlaceTableView.register(UINib(nibName: "EvenstTableViewCell", bundle: nil), forCellReuseIdentifier: "EvenstTableViewCell")
    }

}

extension TookPlaceViewController: UITableViewDelegate{
    
}

extension TookPlaceViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataTookPlace.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "EvenstTableViewCell", for: indexPath) as? EvenstTableViewCell else { return UITableViewCell() }
        cell.delegate = self
        cell.indexPathRowEv = indexPath.row
        cell.configueData(data: dataTookPlace[indexPath.row])
        return cell
    }
}
extension TookPlaceViewController: EvenstDelegate{
    func presetDelegate(state: stateSelected, indexStateEv: Int, indexPathRow: Int) {
        guard let vc = UIStoryboard(name: "PopupViewController", bundle: nil).instantiateViewController(withIdentifier: "PopupViewController") as? PopupViewController else { return }
        vc.stateSe = state
        vc.indexSelectedState = indexStateEv
        vc.indexPathRow = indexPathRow
        vc.delegate = self
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        present(vc, animated: true, completion: nil)
    }
}
extension TookPlaceViewController: PopupDelegate {
    func popupDelegate(events: stateSelected, indexPathRow: Int) {
        dataTookPlace[indexPathRow].state = events
        tookPlaceTableView.reloadData()
    }
}
