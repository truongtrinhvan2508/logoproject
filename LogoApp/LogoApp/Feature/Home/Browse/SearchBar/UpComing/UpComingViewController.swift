//
//  UpComingViewController.swift
//  LogoApp
//
//  Created by TruongTV2 on 4/28/22.
//

import UIKit

class UpComingViewController: UIViewController {

    @IBOutlet weak var upComingTableView: UITableView!
    var dataUpComing: [Events] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }

    func setupTableView(){
        dataUpComing = EventsRepository.Shared.EventsData()
        upComingTableView.delegate = self
        upComingTableView.dataSource = self
        upComingTableView.register(UINib(nibName: "EvenstTableViewCell", bundle: nil), forCellReuseIdentifier: "EvenstTableViewCell")
    }

}

extension UpComingViewController: UITableViewDelegate{
    
}

extension UpComingViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataUpComing.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "EvenstTableViewCell", for: indexPath) as? EvenstTableViewCell else { return UITableViewCell() }
        cell.delegate = self
        cell.indexPathRowEv = indexPath.row
        cell.configueData(data: dataUpComing[indexPath.row])
        return cell
    }
}
extension UpComingViewController: EvenstDelegate{
    func presetDelegate(state: stateSelected, indexStateEv: Int, indexPathRow: Int) {
        guard let vc = UIStoryboard(name: "PopupViewController", bundle: nil).instantiateViewController(withIdentifier: "PopupViewController") as? PopupViewController else { return }
        vc.stateSe = state
        vc.indexSelectedState = indexStateEv
        vc.indexPathRow = indexPathRow
        vc.delegate = self
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        present(vc, animated: true, completion: nil)
    }
}
extension UpComingViewController: PopupDelegate {
    func popupDelegate(events: stateSelected, indexPathRow: Int) {
        dataUpComing[indexPathRow].state = events
        upComingTableView.reloadData()
    }
}
