//
//  LogoModel.swift
//  LogoApp
//
//  Created by TruongTV2 on 4/26/22.
//

import Foundation

struct News{
    let date: String
    let title: String
    let author: String
    let address: String
}

struct Events{
    let date: String
    let quantity: Int
    let title: String
    let description: String
    var state: stateSelected
}

class NewsRepository{
    static let shared = NewsRepository()
    
    func NewsData() -> [News]{
        return [News(date: "20/04/2022", title: " UK ports ‘preparing to host EU customs checks’", author: "TruongTV2", address: "Instagram"),
                News(date: "20/04/2022", title: " UK ports ‘preparing to host EU customs checks’", author: "TruongTV2", address: "Instagram"),
                News(date: "20/04/2022", title: " UK ports ‘preparing to host EU customs checks’", author: "TruongTV2", address: "Instagram"),
                News(date: "20/04/2022", title: " UK ports ‘preparing to host EU customs checks’", author: "TruongTV2", address: "Instagram"),]
    }
}

class EventsRepository{
    static let Shared = EventsRepository()
    
    func EventsData() -> [Events]{
        return [
            Events(date: "21/04/2022", quantity: 20, title: "UK ports ‘preparing to host EU customs checks’ ", description: "When someone has a burn injury, skin is used as a 'natural plaster' to help healing. It helps stop...When someone has a burn injury, skin is used as a 'natural plaster' to help healing. It helps stop...When someone has a burn injury, skin is used as a 'natural plaster' to help healing. It helps stop...When someone has a burn injury, skin is used as a 'natural plaster' to help healing. It helps stop...When someone has a burn injury, skin is used as a 'natural plaster' to help healing. It helps stop...", state: .no),
            Events(date: "21/04/2022", quantity: 20, title: "UK ports ‘preparing to host EU customs checks’ ", description: "When someone has a burn injury, skin is used as a 'natural plaster' to help healing. It helps stop...", state: .notSure),
            Events(date: "21/04/2022", quantity: 20, title: "UK ports ‘preparing to host EU customs checks’ ", description: "When someone has a burn injury, skin is used as a 'natural plaster' to help healing. It helps stop...", state: .no),
            Events(date: "21/04/2022", quantity: 20, title: "UK ports ‘preparing to host EU customs checks’ ", description: "When someone has a burn injury, skin is used as a 'natural plaster' to help healing. It helps stop...", state: .sure),
            Events(date: "21/04/2022", quantity: 20, title: "UK ports ‘preparing to host EU customs checks’ ", description: "When someone has a burn injury, skin is used as a 'natural plaster' to help healing. It helps stop...", state: .no)
        ]
    }
}

class BrowseRepository{
    static let shared = BrowseRepository()
    
    func BrowseData() -> [String]{
        return ["Arts & Culture","Career & Business","Cars & Motorcycles","Community & Environment","Dancing","Education & Learning","Fashion & Beauty","Fitness","Games","LGBT","Movements & Politics","Hobbies & Crafts","Language & Ethnic Identity",]
    }
}
